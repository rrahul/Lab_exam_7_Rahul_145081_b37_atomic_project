<?php
namespace App\Birthday;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;


class Birthday extends DB{
    public $id="";
    public $BirthdayName="";
    public $BirthdayDate="";

    public function __construct(){
        parent::__construct();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {

        if(array_key_exists('BirthdayName',$data))
        {
            $this->BirthdayName = $data['BirthdayName'];
        }
        if(array_key_exists('BirthdayDate',$data))
        {
            $this->BirthdayDate = $data['BirthdayDate'];
        }
    }
    public function  store()
    {

        $query = $this->conn-> prepare("INSERT INTO birthday(BirthdayName, BirthdayDate)
        VALUES(:BirthdayName,:BirthdayDate)");
        $query->execute(array(
            "BirthdayName" => $this->BirthdayName,
            "BirthdayDate" => $this->BirthdayDate,

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ BirthdayName: $this->BirthdayName ] , [ BirthdayDate: $this->BirthdayDate ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ BirthdayName: $this->BirthdayName ] , [ BirthdayDate: $this->BirthdayDate ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }
}