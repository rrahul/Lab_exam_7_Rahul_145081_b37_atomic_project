<?php
namespace App\SummeryOfOrganization;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class SummeryOfOrganization extends DB{
    public $BookId="";
    public $summery_of_organization_name="";
    public $summery_of_organization_details="";

    public function __construct(){
        parent::__construct();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {
        if(array_key_exists('BookId',$data))
        {
            $this->BookId = $data['BookId'];
        }
        if(array_key_exists('summery_of_organization_name',$data))
        {
            $this->summery_of_organization_name = $data['summery_of_organization_name'];
        }
        if(array_key_exists('summery_of_organization_details',$data))
        {
            $this->summery_of_organization_details = $data['summery_of_organization_details'];
        }
    }
    public function  store()
    {

        $query = $this->conn-> prepare("INSERT INTO summery_of_organization(summery_of_organization_name, summery_of_organization_details)
        VALUES(:summery_of_organization_name,:summery_of_organization_details)");

        $query->execute(array(
            "summery_of_organization_name" => $this->summery_of_organization_name,
            "summery_of_organization_details" => $this->summery_of_organization_details,

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ Name: $this->summery_of_organization_name ] , [ Details: $this->summery_of_organization_details ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ Name: $this->summery_of_organization_name ] , [ Details: $this->summery_of_organization_details ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}