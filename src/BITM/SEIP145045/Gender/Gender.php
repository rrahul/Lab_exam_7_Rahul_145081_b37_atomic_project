<?php
namespace App\Gender;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class Gender extends DB{
    public $BookId="";
    public $GenderName="";
    public $GenderValue="";

    public function __construct(){
        parent::__construct();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {
        if(array_key_exists('BookId',$data))
        {
            $this->BookId = $data['BookId'];
        }
        if(array_key_exists('GenderName',$data))
        {
            $this->GenderName = $data['GenderName'];
        }
        if(array_key_exists('GenderValue',$data))
        {
            $this->GenderValue = $data['GenderValue'];
        }
    }
    public function  store()
    {

        $query = $this->conn-> prepare("INSERT INTO gender(GenderName, GenderValue)
        VALUES(:GenderName,:GenderValue)");
        $query->execute(array(
            "GenderName" => $this->GenderName,
            "GenderValue" => $this->GenderValue,

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ GenderName: $this->GenderName ] , [ GenderValue: $this->GenderValue ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ GenderName: $this->GenderName ] , [ GenderValue: $this->GenderValue ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}