<?php
namespace App\City;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class City extends DB{
    public $BookId="";
    public $CityName="";
    public $CityValue="";

    public function __construct(){
        parent::__construct();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {
        if(array_key_exists('BookId',$data))
        {
            $this->BookId = $data['BookId'];
        }
        if(array_key_exists('CityName',$data))
        {
            $this->CityName = $data['CityName'];
        }
        if(array_key_exists('CityValue',$data))
        {
            $this->CityValue = $data['CityValue'];
        }
    }
    public function  store()
    {

        $query = $this->conn-> prepare("INSERT INTO city(CityName, CityValue)
        VALUES(:CityName,:CityValue)");
        $query->execute(array(
            "CityName" => $this->CityName,
            "CityValue" => $this->CityValue,

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ CityName: $this->CityName ] , [ CityValue: $this->CityValue ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ CityName: $this->CityName ] , [ CityValue: $this->CityValue ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}