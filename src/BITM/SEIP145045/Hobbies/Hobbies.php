<?php
namespace App\Hobbies;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class Hobbies extends DB{
    public $BookId="";
    public $HobbiesName="";
    public $HobbiesValue="";

    public function __construct(){
        parent::__construct();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {
        if(array_key_exists('BookId',$data))
        {
            $this->BookId = $data['BookId'];
        }
        if(array_key_exists('HobbiesName',$data))
        {
            $this->HobbiesName = $data['HobbiesName'];
        }
        if(array_key_exists('HobbiesValue',$data))
        {
            $this->HobbiesValue = implode(',', $_POST['HobbiesValue']);

        }
    }
    public function  store()
    {

        $query = $this->conn-> prepare("INSERT INTO hobbies(HobbiesName, HobbiesValue)
        VALUES(:HobbiesName,:HobbiesValue)");
        $query->execute(array(
            "HobbiesName" => $this->HobbiesName,
            "HobbiesValue" => $this->HobbiesValue,

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ HobbiesName: $this->HobbiesName ] , [ HobbiesValue: $this->HobbiesValue ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ HobbiesName: $this->HobbiesName ] , [ HobbiesValue: $this->HobbiesValue ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}