<?php
namespace App\BookTitle;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class BookTitle extends DB{
    public $BookId="";
    public $BookTitle="";
    public $BookAuthor="";

    public function __construct(){
        parent::__construct();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {
        if(array_key_exists('BookId',$data))
        {
            $this->BookId = $data['BookId'];
        }
        if(array_key_exists('BookTitle',$data))
        {
            $this->BookTitle = $data['BookTitle'];
        }
        if(array_key_exists('BookAuthor',$data))
        {
            $this->BookAuthor = $data['BookAuthor'];
        }
    }
    public function  store()
    {

        $query = $this->conn-> prepare("INSERT INTO book_title(BookTitle, BookAuthor)
        VALUES(:BookTitle,:BookAuthor)");
        $query->execute(array(
            "BookTitle" => $this->BookTitle,
            "BookAuthor" => $this->BookAuthor,

        ));
       
        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ BookTitle: $this->BookTitle ] , [ AuthorName: $this->BookAuthor ] <br> Data Has Been Inserted Successfully!</h3></div>");

            }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ BookTitle: $this->BookTitle ] , [ AuthorName: $this->BookAuthor ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}