<?php
namespace App\Email;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class Email extends DB{

    public $EmailName="";
    public $EmailAddress="";

    public function __construct(){
        parent::__construct();
    }
    public function index()
    {
        echo " I am inside of index method";
    }
    public function setData($data = NULL)
    {
        if(array_key_exists('BookId',$data))
        {
            $this->BookId = $data['BookId'];
        }
        if(array_key_exists('EmailName',$data))
        {
            $this->EmailName = $data['EmailName'];
        }
        if(array_key_exists('EmailAddress',$data))
        {
            $this->EmailAddress = $data['EmailAddress'];
        }
    }
    public function  store()
    {

        $query = $this->conn-> prepare("INSERT INTO email(EmailName, EmailAddress)
        VALUES(:EmailName,:EmailAddress)");
        $query->execute(array(
            "EmailName" => $this->EmailName,
            "EmailAddress" => $this->EmailAddress,

        ));

        if($query) {
            Message::message("<div class='alert alert-success' id='msg'><h3 align='center'>[ EmailName: $this->EmailName ] , [ EmailAddress: $this->EmailAddress ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div class='alert alert-danger' id='msg'><h3 align='center'>[ BookTitle: $this->BookTitle ] , [ AuthorName: $this->BookAuthor ] <br> Data Has Not Been Inserted Successfully!</h3></div>");

        }
        Utility::redirect("create.php");
    }




}